<?php

/*
 * Template Name: Stappenplan
 * Description: Stappenplan voor prijsindicatie.
 */

get_header();
?>
<script>
function calculateForm () {
    console.log("calculating");
    let calcform = document.forms.calc;
    let wat = document.calc.wat.value;
    let waar = 1;
    
    if(document.calc.grammatica.checked == true) {
        waar += 1;
    }
    if(document.calc.structuur.checked == true ) {
        waar += 1;
    }
    if(document.calc.begrijpelijkheid.checked == true ) {
        waar += 1;
    }
    let wanneer = document.calc.wanneer.value;
    let hoeveel = document.calc.amount.value;
    console.log('waar:' + waar);
    console.log('wanneer:' + wanneer);
    console.log('hoeveel:' + hoeveel);
    let cpw = 0.003;
    let total = 0;
    let sub1 = 0;
    let sub2 = 0;
    let sub3 = 0;
    sub1 = waar + wanneer;
    sub2 = sub1 * hoeveel;
    sub3 = sub2 * cpw;
    console.log('sub1: ' + sub1 + ' sub2: ' + sub2 + ' sub3: ' + sub3 );

    total = ((waar + wanneer) * hoeveel) * 0.003;

    console.log('totaal:' + total);   

};


</script>
<div id="primary" class="content-area">
		<main id="main" class="site-main">

<div class="entry-content">
<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );


			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

    <form action="#" method="post" id="calc" name="calc">
    <?php wp_nonce_field( 'faire-don', 'cagnotte-verif' ); ?>

    <?php 
        $wat = array( 
            "Vacaturetekst" => "vacaturetekst",
            "Rapport" => "rapport",
            "Nieuwsbrief" => "nieuwsbrief",
            "Websitetekst" => "websitetekst",
            "Vertaalde tekst (Nederlands" => "vertaalde_tekst",
            "Blog of Artikel" => "blog_of_artikel",
            "Whitepaper" => "whitepaper",
            "Magazine" => "magazine",
            "Anders" => "anders",
        );
        $waar = array(
            array("Spelling en grammatica", "spelling", 1, 0),
            array("Zinsopbouw, woordkeuze en stijl", "grammatica", 2, 1),
            array("Structuur en opbouw tekst", "structuur", 3, 1),
            array("Begrijpelijkheid", "begrijpelijkheid", 2, 1),
        );
        $wanneer = array(
            array("Het liefst vandaag nog!", 5),
            array("Over drie dagen", 4),
            array("Over een week", 3),
            array("Over een maand", 2),
        );
?>
    <div>
        <fieldset>
        <legend for="wat">Wat wil je nagekeken hebben?</legend>
      
      <?php
            foreach( $wat as $k => $v ) {
                ?>
                <div>
                    <input type="radio" 
                            name="wat" 
                            id="<?php echo $v; ?>" 
                            value="<?php echo $v; ?>">
                    <label for="<?php echo $v; ?>"><?php echo $k; ?></label>
                </div>
                <?php
            }
            ?>
         
        </fieldset>
    </div>

    <div>
        <fieldset>
            <legend>Waar wil je dat ik op let? *</legend>
            <?php
                foreach( $waar as $v1) {
                    ?>
                <div>
                    <input type="checkbox" 
                        <?php if($v1[3] == 0) echo 'checked="checked" disabled="disabled" ';?>
                            name="<?php echo $v1[1]; ?>" 
                            id="<?php echo $v1[1]; ?>" 
                            value="<?php echo $v1[1]; ?>">
                    <label for="<?php echo $v1[1]; ?>"><?php echo $v1[0]; ?></label>
                </div>
    
                <?php
                }
                ?>
        </fieldset>
    </div>
    
    <div>
        <fieldset>
            <legend for="wanneer">Wanneer wil je het terug hebben?</legend>
            <?php
                foreach( $wanneer as $v1) {
                    ?>
                <div>
                    <input type="radio" 
                            name="wanneer" 
                            id="<?php echo $v1[1]; ?>" 
                            value="<?php echo $v1[1]; ?>">
                    <label for="<?php echo $v1[1]; ?>"><?php echo $v1[0]; ?></label>
                </div>
    
                <?php
                }
                ?>
        </fieldset>
    </div>


    <div>
        <label for="amount">Hoeveel woorden bevat je tekst?</label><br>
        <input id="amount" name="amount" type="number" value="amount">
    </div>

        <input onclick="calculateForm()" type="submit" name="submit" value="bereken">
    </form>
    <?php 
         if (isset($_POST['submit'])){
            $waar = $_POST['waar'];
            $wanneer = $_POST['wanneer'];
            $amount = $_POST['amount'];
            if($waar == "spelling" ) {
                $waar = 4;
            }
            echo 'waar:' . $waar . '<br>';
            echo 'wanneer:' . $wanneer . '<br>';
            echo 'hoeveel:' . $amount . '<br>';
            $cpw = 0.03;
            echo "(((" . $waar . " + " . $wanneer . ") * " . $amount . ") / 3 * " . $cpw;
            $total = ((($waar + $wanneer) * $amount) / 3) * $cpw;
            echo 'totaal: &euro;' . round($total, 0); 
             
        }
    ?>
</div>
</main>
</div>
<?php
get_footer();
?>

